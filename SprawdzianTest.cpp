//
// Created by qiubix on 13.01.18.
//

#include "gmock/gmock.h"
#include "Zdanie.h"
#include "funkcje.h"
#include <fstream>

using namespace std;
using namespace testing;

TEST(Sprawdzian, RunsEmptyTest) {
    ASSERT_THAT(2, Eq(2));
}

TEST(FileLoader, shouldLoadAllSentencesFromFile) {
    // given
    string firstLine = "First line content";
    string secondLine = "Second line content";
    string thirdLine = "Third line content";
    ofstream testFile;
    testFile.open("testFile.txt");
    testFile << firstLine << "\n" << secondLine << "\n" << thirdLine;
    testFile.close();

    // when
    vector<string> result = wczytajWszystkieZdania("testFile.txt");

    // then
    ASSERT_THAT(result, SizeIs(3));
    ASSERT_THAT(result[0], Eq(firstLine));
    ASSERT_THAT(result[1], Eq(secondLine));
    ASSERT_THAT(result[2], Eq(thirdLine));
}

TEST(SentenceMapper, shouldMapEnglishSentencesToPolishOnes)
{
    // given: a file with polish sentences and english solutions
    string polishFilePath = "zdania-polskie.txt";
    string englishFilePath = "zdania-angielskie.txt";

    // when: reading from both files
    vector<Zdanie> sentences = polaczZdaniaZOdpowiedziami(polishFilePath, englishFilePath);

    // then: polish sentences should be correctly matched to english solutions
    ASSERT_THAT(sentences, Not(IsEmpty()));
    auto polishSentences = wczytajWszystkieZdania(polishFilePath);
    auto englishSentences = wczytajWszystkieZdania(englishFilePath);
    ASSERT_THAT(polishSentences.size(), Eq(englishSentences.size()));
    Zdanie firstSentence = sentences[0];
    ASSERT_THAT(firstSentence.polskieZdanie(), Eq(polishSentences[0]));
    ASSERT_THAT(firstSentence.angielskieZdanie(), Eq(englishSentences[0]));
    Zdanie lastSentence = sentences.back();
    ASSERT_THAT(lastSentence.polskieZdanie(), Eq(polishSentences.back()));
    ASSERT_THAT(lastSentence.angielskieZdanie(), Eq(englishSentences.back()));
}

TEST(SentenceMapper, shouldThrowExceptionWhenDatafilesAreIllFormatted) {
    // given
    string polishFilePath = "zdania-polskie.txt";
    auto illFormedEnglishFilePath = "testFile.txt";
    string firstLine = "First line content";
    ofstream testFile;
    testFile.open(illFormedEnglishFilePath);
    testFile << firstLine << "\n";
    testFile.close();

    // when & then
    ASSERT_THROW(polaczZdaniaZOdpowiedziami(polishFilePath, illFormedEnglishFilePath),
                 NiepoprawnePlikiZDanymi);
}

TEST(SentencePicker, shouldSelectRandomSequenceFromSet) {
    // given
    string polishFilePath = "zdania-polskie.txt";
    string englishFilePath = "zdania-angielskie.txt";

    // when
    Zdanie randomSentence = wylosujZdanie(polishFilePath, englishFilePath);

    // then
    auto polishSentences = wczytajWszystkieZdania(polishFilePath);
    ASSERT_THAT(polishSentences, Contains(Eq(randomSentence.polskieZdanie())));
}

TEST(SentenceChecker, shouldReturnEmptyStringWhenGivenCorrectAnswer) {
    // given
    string expectedAnswer = "This is a correct answer.";
    Zdanie sentence = Zdanie("polish", expectedAnswer);

    // when
    string correctAnswer =  "This is a correct answer.";
    string markedErrors = sentence.sprawdzOdpowiedz(correctAnswer);

    // then
    ASSERT_THAT(markedErrors, Eq(""));
}

TEST(SentenceChecker, shouldMarkAllMistakesInSentenceWithTheSameLength) {
    // given
    string correctAnswer =  "This is a correct answer.";
    string wrongAnswer =    "Adg wis a borweeta nswer.";
    string expectedErrors = "^^^^^     ^  ^ ^ ^^      ";
    Zdanie sentence = Zdanie("polish", correctAnswer);

    // when
    string markedErrors = sentence.sprawdzOdpowiedz(wrongAnswer);

    // then
    ASSERT_THAT(markedErrors, Eq(expectedErrors));
}

TEST(SentenceChecker, shouldMarkAllMistakesInSentenceShorterThanCorrectAnswer) {
    // given
    string correctAnswer =  "Correct answer.";
    string wrongAnswer =    "Xorrect";
    string expectedErrors = "^      ^^^^^^^^";
    Zdanie sentence = Zdanie("polish", correctAnswer);

    // when
    string markedErrors = sentence.sprawdzOdpowiedz(wrongAnswer);

    // then
    ASSERT_THAT(markedErrors, Eq(expectedErrors));
}

TEST(SentenceChecker, shouldMarkAllMistakesInSentenceLongerThanCorrectAnswer) {
    // given
    string correctAnswer =  "Correct answer.";
    string wrongAnswer =    "Xorrect axswer but longer.";
    string expectedErrors = "^        ^    ^^^^^^^^^^^^";
    Zdanie sentence = Zdanie("polish", correctAnswer);

    // when
    string markedErrors = sentence.sprawdzOdpowiedz(wrongAnswer);

    // then
    ASSERT_THAT(markedErrors, Eq(expectedErrors));
}

TEST(ErrorsPercentage, shouldCalculateErrorsPercentage) {
    ASSERT_THAT(obliczProcentBledow("      "), DoubleEq(0.0));
    ASSERT_THAT(obliczProcentBledow(" ^"), DoubleEq(50.0));
    ASSERT_THAT(obliczProcentBledow("^ "), DoubleEq(50.0));
    ASSERT_THAT(obliczProcentBledow("^ ^^"), DoubleEq(75.0));
    ASSERT_THAT(obliczProcentBledow("  ^     "), DoubleEq(12.5));
    ASSERT_THAT(obliczProcentBledow("^^^^^^^^"), DoubleEq(100.0));
}

