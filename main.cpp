#include <iostream>
#include <limits>
#include "Zdanie.h"
#include "funkcje.h"

using namespace std;


int main(int argc, char** argv) {

    cout << "Witaj w automatycznym teście z języka angielskiego!" << endl;
    cout << "1 - rozpocznij test" << endl;
    cout << "2 - wyjscie" << endl;

    char akcja;
    while (true) {
        cin >> akcja;
        if (akcja == '1') {
            break;
        } else if (akcja == '2') {
            return 0;
        } else {
            continue;
        }
    }

    cin.clear();
    cin.ignore(numeric_limits<streamsize>::max(), '\n');


    cout << "Wczytywanie danych z bazy...\n";
    string sciezkaDoPolskiegoPliku = "zdania-polskie.txt";
    string sciezkaDoAngielskiegoPliku = "zdania-angielskie.txt";

    cout << "Losowanie zdania...\n";
    Zdanie zdanieTestowe = wylosujZdanie(sciezkaDoPolskiegoPliku, sciezkaDoAngielskiegoPliku);

    cout << "**************************************************\n";
    cout << "*===================== TEST =====================*\n";
    cout << "* Przetłumacz ponizsze zdanie na jezyk angielski *\n";
    cout << "*                                                *\n";
    cout << "**************************************************\n";
    cout << zdanieTestowe.polskieZdanie() << endl;
    string odpowiedz;
    getline(cin, odpowiedz);
    cout << "Twoja odpowiedz to: " << odpowiedz << endl;
    cout << "Poprawna odpowiedz: " << zdanieTestowe.angielskieZdanie() << endl;
    auto zaznaczoneBledy = zdanieTestowe.sprawdzOdpowiedz(odpowiedz);
    cout << "Bledy:              " << zaznaczoneBledy << endl;
    cout << "Procentowa liczba bledow: " << obliczProcentBledow(zaznaczoneBledy) << "%\n";

    return 0;
}
