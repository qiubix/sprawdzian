# Sprawdzian

## Wymagania
+ Baza zdań w języku polskim w pliku
+ Każde zdanie w osobnej linii
+ Losowanie zdania do rozwiązania
+ Wyświetlenie wylosowanego zdania na ekran
+ Wczytywanie zdania z klawiatury
- Ukrycie wpisywanego tekstu na ekranie
+ Wyświetlenie wpisanego zdania po naciśnięciu klawisza enter
+ Zaznaczenie wszystkich błędów: litery na niewłaściwych pozycjach
- Wyświetlenie procentowej wartości błędu: jaka część wpisanych liter na właściwych pozycjach
- Wyświetlenie czasu wpisywania zdania
