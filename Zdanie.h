#ifndef ZDANIE_H
#define ZDANIE_H

#include <vector>
#include <string>
#include <fstream>

using namespace std;

class Zdanie {
    string _polskie;
    string _angielskie;

public:
    Zdanie(const string& polskie, const string& angielskie);

    string polskieZdanie();
    string angielskieZdanie();

    string sprawdzOdpowiedz(const string &odpowiedz);

private:
    string porownajZdania(const string &krotszeZdanie, const string &dluzszeZdanie) const;
};


#endif //ZDANIE_H
