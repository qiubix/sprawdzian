#include "funkcje.h"

vector<string> wczytajWszystkieZdania(string sciezkaDoPliku) {
    ifstream plik(sciezkaDoPliku);
    auto linieWPliku = vector<string>();
    string linia;
    if (plik.is_open()) {
        while (getline(plik, linia)) {
            linieWPliku.push_back(linia);
        }
        plik.close();
    }
    return linieWPliku;
}

vector<Zdanie> polaczZdaniaZOdpowiedziami(const string &sciezkaDoPolskiegoPliku, const string &sciezkaDoAngielskiegoPliku) {
    auto polskieZdania = wczytajWszystkieZdania(sciezkaDoPolskiegoPliku);
    auto angielskieZdania = wczytajWszystkieZdania(sciezkaDoAngielskiegoPliku);
    if (polskieZdania.size() != angielskieZdania.size()) {
        throw NiepoprawnePlikiZDanymi();
    }

    auto zdania = vector<Zdanie>();
    for (int i=0; i<polskieZdania.size(); i++) {
        zdania.push_back(Zdanie(polskieZdania[i], angielskieZdania[i]));
    }
    return zdania;
}

unsigned long generujLosowaLiczbe(unsigned long zakres) {
    srand(time(NULL));
    return rand() % zakres;
}

Zdanie wylosujZdanie(const string &sciezkaDoPolskiegoPliku, const string &sciezkaDoAngielskiegoPliku) {
    vector<Zdanie> zdania = polaczZdaniaZOdpowiedziami(sciezkaDoPolskiegoPliku, sciezkaDoAngielskiegoPliku);
    unsigned long indeksLosowegoZdania = generujLosowaLiczbe(zdania.size() - 1);
    return zdania[indeksLosowegoZdania];
}

double obliczProcentBledow(const string &markedErrors) {
    const auto answerLength = markedErrors.length();
    int numberOfErrors = 0;
    for (char character : markedErrors) {
        if (character == '^') {
            ++numberOfErrors;
        }
    }
    const auto result = (double) numberOfErrors * 100 / answerLength;
    return result;
}
