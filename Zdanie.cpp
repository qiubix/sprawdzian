//
// Created by qiubix on 13.01.18.
//

#include <fstream>
#include <sstream>
#include "stdlib.h"
#include "time.h"
#include "Zdanie.h"


Zdanie::Zdanie(const string &polskie, const string &angielskie) {
    this->_polskie = polskie;
    this->_angielskie = angielskie;
}

string Zdanie::polskieZdanie() {
    return _polskie;
}

string Zdanie::angielskieZdanie() {
    return _angielskie;
}

string Zdanie::sprawdzOdpowiedz(const string &odpowiedz) {
    if (odpowiedz == _angielskie) {
        return "";
    }
    if (odpowiedz.length() <= _angielskie.length()) {
        return porownajZdania(odpowiedz, _angielskie);
    } else {
        return porownajZdania(_angielskie, odpowiedz);
    }
}

string Zdanie::porownajZdania(const string &krotszeZdanie, const string &dluzszeZdanie) const {
    stringstream bledy;
    for (int i=0; i<krotszeZdanie.length(); ++i) {
            if (krotszeZdanie[i] == dluzszeZdanie[i]) {
                bledy << " ";
            } else {
                bledy << "^";
            }
        }
    for (int i=0; i < dluzszeZdanie.length() - krotszeZdanie.length(); i++) {
            bledy << "^";
        }
    return bledy.str();
}
