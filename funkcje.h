
#ifndef SPRAWDZIAN_FUNKCJE_H
#define SPRAWDZIAN_FUNKCJE_H

#include "Zdanie.h"

class NiepoprawnePlikiZDanymi : public exception {
    virtual const char* what() const throw() {
        return "Niepoprawne pliki danych!\nPowinno być tyle samo zdań polskich co angielskich tłumaczeń";
    }
};

vector<string> wczytajWszystkieZdania(string sciezkaDoPliku);

vector<Zdanie> polaczZdaniaZOdpowiedziami(const string &sciezkaDoPolskiegoPliku, const string &sciezkaDoAngielskiegoPliku);

unsigned long generujLosowaLiczbe(unsigned long zakres);

Zdanie wylosujZdanie(const string &sciezkaDoPolskiegoPliku, const string &sciezkaDoAngielskiegoPliku);

double obliczProcentBledow(const string &markedErrors);

#endif //SPRAWDZIAN_FUNKCJE_H
